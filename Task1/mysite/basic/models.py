from django.db import models
# from django.core.urlresolvers import reverse
from django.conf import settings
from phonenumber_field.modelfields import PhoneNumberField
# Create your models here.

# alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')

class Profile(models.Model):
	user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        null=True,
        on_delete=models.CASCADE,
    )
	username = models.CharField(max_length=10,unique=True)
	# personal = models.IntegerField()
	# landline = models.IntegerField(blank=True)
	personal = PhoneNumberField(blank=False,null=True)
	landline = PhoneNumberField(blank=True, null=True)

	def __str__(self):
		return f"{self.username}"

class Address(models.Model):
	title = models.CharField(max_length=200)
	addr_line_one = models.CharField(max_length=50,null=True) 
	addr_line_two = models.CharField(max_length=50,null=True)
	locality = models.TextField(max_length=50)
	pincode = models.IntegerField()
	city = models.TextField(max_length=50)
	state = models.TextField(max_length=50)
	profile = models.ForeignKey(
        "Profile",
        null=True,
        on_delete=models.CASCADE
    )
