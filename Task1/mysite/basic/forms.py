from django import forms 
from .models import Address,Profile

class ProfileForm(forms.ModelForm):
	class Meta:
		model = Profile
		fields = [
			# 'user',
			'username',
			'personal',
			'landline'
		]
	def clean(self):
		super(ProfileForm,self).clean()
		
		username = self.cleaned_data.get('username')
		personal = self.cleaned_data.get('personal')
		landline = self.cleaned_data.get('landline')
		print(*self.cleaned_data.keys())
		
		if len(str(personal)) > 13:
			self._errors['personal'] = self.error_class([ 
                'Maximum 13 characters required'])
		if len(str(landline)) > 12:
			self._errors['landline'] = self.error_class([ 
                'Maximum 12 characters required'])

		return self.cleaned_data

class AddressForm(forms.ModelForm):
	class Meta:
		model = Address
		fields = [
			'title',
			'addr_line_one',
			'addr_line_two',
			'locality',
			'pincode',
			'city',
			'state',
			# 'profile'
		]

	def clean(self):
		super(AddressForm,self).clean()
		
		title = self.cleaned_data.get('title')
		address_line_one = self.cleaned_data.get('addr_line_one')
		address_line_two = self.cleaned_data.get('addr_line_two')
		locality = self.cleaned_data.get('locality')
		pincode = self.cleaned_data.get('pincode')
		city = self.cleaned_data.get('city')
		state = self.cleaned_data.get('state')
		# profile = self.cleaned_data.get('profile')
		print(*self.cleaned_data.keys())
		
		if len(title) > 10:
			self._errors['title'] = self.error_class([ 
                'Maximum 10 characters required'])
		if len(address_line_one) > 50:
			self._errors['address_line_one'] = self.error_class([ 
                'Maximum 50 characters required'])
		if len(address_line_two) > 50:
			self._errors['address_line_two'] = self.error_class([ 
                'Maximum 50 characters required'])
		if len(str(pincode)) > 6 or len(str(pincode))<6:
			self._errors['pincode'] = self.error_class([ 
                'Pincode should be 6 characters long'])
		return self.cleaned_data
