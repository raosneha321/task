from django.conf.urls import url
from django.urls import path
from basic import views
from django.views.generic import TemplateView

urlpatterns = [
	# path('',TemplateView.as_view(template_name='basic/login.html')),
        path('',views.home,name='home'),
	url(r'^about/$',views.AboutView.as_view(),name='about'),
	url(r'^home/$',views.home,name='home'),
	url(r'^profile/$',views.profile,name='profile'),
	url(r'^add_profile/$',views.add_profile,name='add_profile'),
	url(r'^login/$',views.login,name='login'),
	url(r'^logout/$',views.logout,name='logout'),
	url(r'^address/$',views.address,name='address'),
	url(r'^add_address/$',views.add_address,name='add_address'),
	url(r'^addr_details/$',views.addr_details,name='addr_details'),
	path('delete/<str:id>',views.delete,name='delete'),
]
