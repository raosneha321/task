from .models import Address,Profile
from django.shortcuts import render,redirect,get_object_or_404,HttpResponse
from django.contrib import auth
from django.views.generic import TemplateView
from django.contrib.auth.models import User
from .forms import AddressForm,ProfileForm
# Create your views here.

def login(request):
    return render(request, "basic/login.html")

def logout(request):
	auth.logout(request)
	return redirect('home')

def address(request):
	return render(request,"basic/add_address.html")

def profile(request):
	# try:
	# u = User.objects.get(id=request.user.id)
	try:
		obj = Profile.objects.get(user=request.user)
		if obj.username is None:
			raise Profile.DoesNotExist()
		else:
			return redirect('home')
	except Profile.DoesNotExist:
		return render(request,"basic/user_profile.html")
	# return redirect('home')
	

def add_profile(request):
	# u = request.POST['username']
	# p = request.POST['personal']
	# l = request.POST['landline']

	# x = Profile(username=u,user=User.objects.get(id=request.user.id),personal=p,landline=l)
	# x.save()

	details = ProfileForm(request.POST)

	if details.is_valid():
		# x = Address(title=t,addr_line_one=l1,addr_line_two=l2,locality=l,pincode=p,city=c,state=s,profile=pr)
		# x.save()
		# print("Address added")
		form = details.save(commit=False)
		form.user = request.user
		form.save()
		return redirect('home')
	else:
		for field in details:
			for msg in field.errors:
				print(msg,field)
		return render(request,'basic/user_profile.html',{'form':details})

def add_address(request):
	# t = request.POST['title']
	# l1 = request.POST['lineone']
	# l2 = request.POST['linetwo']
	# l = request.POST['locality']
	# p = request.POST['pin']
	# c = request.POST['city']
	# s = request.POST['state']
	# pr = Profile.objects.get(user=request.user)

	details = AddressForm(request.POST)

	if details.is_valid():
		# x = Address(title=t,addr_line_one=l1,addr_line_two=l2,locality=l,pincode=p,city=c,state=s,profile=pr)
		# x.save()
		# print("Address added")
		form = details.save(commit=False)
		form.profile = Profile.objects.get(user=request.user)
		form.save()
		return redirect('home')
	else:
		for field in details:
			for msg in field.errors:
				print(msg,field)
		return render(request,'basic/add_address.html',{'form':details})

def delete(request,id=None):
	# obj = get_object_or_404(Address,id=id)
	# if request.method=='POST':
	# 	obj.delete()
	# 	return redirect('home')
	# context = {
	# 	"object":obj
	# }
	# return render(request,"basic/delete_addr.html",context)

	address = Address.objects.get(id=id)
	address.delete()
	return redirect('home')

def addr_details(request):
	addresses = Address.objects.all()
	return render(request,"basic/list_address.html",{'addresses':addresses})

def home(request):
	# if request.user.is_authenticated:
	# 	request.session['user_name']=Profile.objects.get(user=request.user).username
	return render(request,"basic/home.html")
	
class AboutView(TemplateView):
	template_name = 'about.html'

# class LoginView(TemplateView):
# 	template_name = 'login.html'
