## Structure of project

The folder Task1 consists of two sub folders - mysite and basic which are two different django projects.

mysite has two separate folders - mysite and basic 

basic is the app folder which consists of views,models,templates and urls

Templates folder consist of all html files

Static folder consists of all css and js files

Migrations folder consist of table structures.

## Technology Stack

Django v3.1.2

Sqlite database

HTML 

CSS

JavaScript

Bootstrap

## Features 

Login using google signin

Add multiple addresses

Set username

Add personal and landline contact number

Frontend form validation 

Backend form validation

Logout

## Commands 

Clone the repository to get codes at your local machine then run the commands by changing directory to mysite project in your command prompt

To create project -> django-admin startproject name

To create app -> python manage.py startapp name

To create virtual environment -> conda create --name envt_name django

To start virtual environment -> activate envt_name

To create entire db for our classes defined in models.py -> python manage.py migrate

To register changes in app done with migrate -> python manage.py makemigrations

To start the server -> python manage.py runserver

The server will open at http://127.0.0.1:8000/ or http://localhost:8000/

To check the data stored in db from frontend forms access admin panel of django at http://127.0.0.1:8000/admin/
For accessing admin panel you must have a username and password. Hence you need to create a superuser before accessing admin panel

To create a superuser -> python manage.py createsuperuser 